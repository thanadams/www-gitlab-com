---
layout: markdown_page
title: "Database Team"
---

## Common Links

- [Chat channel](https://gitlab.slack.com/archives/database); please use the `#database` chat channel for questions that don't seem appropriate to use the issue tracker for.

## Focus of Database Team

The Database team focuses on database-specific areas of GitLab and the infrastructure of GitLab.com to enable GitLab
to scale with increasing number of users and projects. This includes various
techniques including:

* Sharding at the application-level or with tools such as [Citus Data](https://www.citusdata.com/)
* Providing tools for the development team to test migrations before they hit production
* Creating tools and procedures necessary to ensure all migrations can be performed without downtime
* Providing monitoring for slow queries
* Tuning PostgreSQL and MySQL for optimal usage

For further details, see the [current vacancy for a Database Specialist](/jobs/specialist/database/).
