---
layout: markdown_page
title: People Operations Vision
---

## Happy Team / Culture

1. Deliver programs to support positive influences on people’s lives.
1. Establish an effective code of conduct in support of our vision and values.
1. Implement policies people perceive to be fair, valuable, and necessary.
1. Maintain a healthy culture.
1. Visibility in eNPS across the company on a rolling basis, per division and team.
  1. If eNPS is off asked more detailed questions, while staying concise and DRY.
  1. Add twice yearly [12 question engagement survey](http://www.goalbusters.net/uploads/2/2/0/4/22040464/gallup_q12.pdf) for more robust engagement data.
  1. Be respectful of people's time, only ask detailed and frequent queries when actionable/needed.
1. Build inclusive policies and programs.

## Industry Leading Hiring Practices

1. Attract and hire industry leaders and top-tier talent.
1. Deliver an awesome candidate experience.
1. Make effective hiring decisions.
  1. Hire the right person for the right role at the right time.
  1. Consider local pay in sourcing and hiring.
1. Data driven hiring process.
1. Sourcing for all positions:
  1. 80% outbound
  1. 20% inbound
1. Actively hire globally.
1. Define a candidate centric interview process.
  1. Review Interview questions with a team when creating the position.
  1. Regularly evaluate interview questions with team for effectiveness based on data from previous applicants and competencies required for the position.
  1. Evaluate interviewers for effectiveness based on data from previous applicants and hired team members. Everyone at GitLab should be able to [conduct interviews](https://gitlab.com/gitlab-com/peopleops/issues/305).

## Establish Employer Brand

1. Become a preferred employer.
1. Leverage brand awareness, established channels and network to promote and communicate the successes and challenges of a fully distributed model.
1. Organize a conference.
1. Be globally recognized as the first and the largest remote only company and the thought leader.
1. Increase recognition and distinction from competitors.

## Onboarding

1. Ensure every team member has an engaging, educational, and enjoyable (fun/exciting) onboarding experience (onboarding NPS).
1. Measure program successes, weaknesses, and points of failure.
1. Offer people [money to leave](https://www.bloomberg.com/news/articles/2008-09-16/why-zappos-offers-new-hires-2-000-to-quitbusinessweek-business-news-stock-market-and-financial-advice).
1. Detect underperformance during onboarding; start a PIP early if there are warning signs.
1. Incorporate more social team connection and automation into onboarding process.
  1. Provide training about git on the command line that is interactive instead of intimidating.
  1. Create opportunities for more personal connection and investment in the individual.

## Way of Working

1. Be as flexible as possible while keeping the organization safe from harm.
1. Using product processes (sprints).
1. Using our product (issue boards).
1. Create and deliver product aligned with the business.
1. Default to [transparency](https://about.gitlab.com/handbook/values/#sts=Transparency). Only make something confidential if it is explicitly so.

## Legal / Compliance / Governance

1. Implement and maintain statutory reporting, training, and communications.
1. Establish a clear set of governance and procedural obligations to maintain compliance with all local, state, federal and country specific mandates.
1. Hire people as employees in many locations.
1. Leverage visa's as a recruiting and retention tool (unique that you can prove by contributing (rather than by credentials), and that we will hire you in other countries).
1. Living campus in the Netherlands for recent immigrants.

## Learning & Development

1. Provide development opportunities at all levels for all divisions.
1. Create career paths for people to clearly see future opportunities, describe how to get there and reasonable timelines.
1. Create leadership development programs regarded as best in class and a reason to join the company.
1. Lightweight process to add courses.
1. Have gamified / visible what courses you do.
1. Used extensively by wider community (users, contributors, resellers, applicants).
1. Have tests.
1. Have support.
1. Enormous volume of new and updated courses.

## Compensation

1. Develop a model regarded as fair, competitive, and attractive for each respective cohort (Individual, Manager, Executive, Sales).
1. Reward people for results.
1. Encourage model behaviors using incentive programs.
1. Pay what we need to get/keep great people.
1. Preference for low cost locations; communicate this as a representation of our values (Frugality, but also Diversity).

## Benefits

1. Evaluate benefit plans in countries where we have an entity or payroll solution.
1. Develop competitive country specific benefit programs to attract and retain the best people.
1. Don’t just offer the minimum package possible, truly demonstrate care for the health and welfare of team members.
1. Create wellness program that can be implemented remotely for all team members.
1. Attractive and responsible employer that provides freedom and equality while getting the maximum out of every dollar spent.
1. Freedom for the team member (no 401k employer contribution, etc.)
  1. Team member interested 401k: 100 base, 10 payment, 10 employer match, 110 total
  1. Team member not interested: 100 base, 100 total
  1. Fair solution: 110 base for both, provide resources for everyone without a 401k to talk to a financial advisor.
1. Freelancers and employees treated as similar as possible within the law.
