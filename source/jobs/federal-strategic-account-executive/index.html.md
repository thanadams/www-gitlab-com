---
layout: job_page
title: "Federal Strategic Account Executive"
---

GitLab is seeking an experienced, high-energy federal sales professional with a proven track record of over achieving quota. As the individual who represents GitLab and drives the revenue, this person must be a closer who can create a large pipeline of business within a short period of time and work with existing clients as well as new logo opportunities. This person should be located in the Washington, DC area.

## Responsibilities

- Conduct sales activities including prospecting and developing opportunities in large/strategic accounts
- Ensure the successful rollout and adoption of Gitlab products through strong account management activities and coordination with pre-and-post sales engineering and support resources
- Travel as necessary to accounts in order to develop relationships and close large opportunities
- Generate qualified leads and develop new customers in conjunction with our strategic channel partners in exceeding quota.
- Expand knowledge of industry as well as the competitive posture of the company
- Prepare activity and forecast reports as requested
- Update and maintain Sales’ database as appropriate
- Assist sales management in conveying customer needs to product managers, and technical support staff
- Utilizing a consultative approach, discuss business issues with prospect and develop a formal quote, a written sales proposal or a formal sales presentation addressing their business needs.
- Respond to RFP's and follow up with prospects.
- Develop an account plan to sell to customers based on their business needs.
- Build and strengthen the business relationship with current accounts and new prospects.
- Recommend marketing strategies.

## Requirements

- Able to provide high degree of major account management and control
- Work under minimal supervision on complex projects.
- Strong interpersonal skills and ability to excel in a team oriented atmosphere
- Strong written/verbal communications skills
- 5+ years successful quota attainment in the Federal Space
- Very motivated and goal-oriented
- Must want a career-oriented environment that is both fun and professional.
- Strong customer service orientation and ability to develop and maintain relationships
- Preferred experience with Git, Software Development Tools, Application Lifecycle Management
- You share our values, and work in accordance with those values.
